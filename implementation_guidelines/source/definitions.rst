.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. |main_project_name| replace:: Oniro-Based Project
.. |company_entity| replace:: ACME Inc.
.. |company_entity_shortname| replace:: ACME
.. |main_policy_url| replace:: https://www.example.com/
.. |openchain_specification| replace:: `OpenChain Specification 2.0 <https://wiki.linuxfoundation.org/_media/openchain/openchainspec-2.0.pdf>`__
.. |openchain_specification_license| replace:: `CC-BY-4.0 <https://creativecommons.org/licenses/by/4.0/>`__
.. |email_for_external_inquiries| replace:: inquiries@example.com
