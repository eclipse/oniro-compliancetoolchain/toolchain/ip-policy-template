.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _requirements:

Requirements
############

.. _program-foundation:

Program Foundation
******************

.. _policy:

Policy
======

All joining Software Staff to |company_entity_shortname| will be made aware of the existence of the Open Source Policy and of these Implementation Guidelines, and associated training policy and its location during the induction process. This will be recorded on the Induction Checklist of HR system.

.. _sec-roles_responsibilities:

Roles and responsibilities
==========================

A general list of useful contacts with corresponding operations and areas related to |company_entity_shortname| can be found in the Appendix to these Guidelines.

A list of roles with corresponding primary responsibilities, main competencies and time requirement for the different participants in the Program can be found in the table below:

.. tabularcolumns:: |p{2cm}|p{6cm}|p{6cm}|p{2cm}|

.. csv-table:: Primary roles and responsibilities
   :file: roles.csv
   :header-rows: 1
   :class: longtable

All participants in the Program (including Software Staff) must undertake training covering the competencies required for their role, and at a minimum basic training. You can find details of the training requirements for each role in the training requirement page of the Appendix to these Guidelines. Such training should be held every 12 months unless there is a major update to the Policy or to any implementation guideline so that it may be triggered more frequently.

Every member of Software Staff in the Program will be assessed and records of the assessment will be found in the learning platform indicated in the Appendix.

.. _awareness:

Awareness
=========

Open Source Compliance objectives are set out in the introduction to these Policy Implementation Guidelines. It is important that |company_entity_shortname| adheres to the Open Source Policy and to these Guidelines. Failure to do so may lead to:

-  legal claims from the holders of copyright or other intellectual property rights in code we use
-  jeopardizing the relationship with developer and user communities, and losing their support
-  claims from customers
-  inadvertent release of proprietary code
-  loss or reputation
-  loss of revenue
-  breach of contract with suppliers and customers

The company training and assessment program will cover the objectives of each Program in which one participates, their role within the Program, and implications to the Company and to individuals for non-conformance. Evidence of the assessment can be found in the learning platform indicated in the Appendix.

.. _program-scope:

Program Scope
=============

These Open Source Policy Implementation Guidelines cover and apply to all open source software developed and made available by |company_entity_shortname|, including all activities related to the development, improvement, testing and release of |main_project_name| and related projects.

.. _license-obligations:

License Obligations
===================

Obligations, restrictions and rights granted by most prevalent open source Licenses (as those listed in ‘`Choose a License <https://choosealicense.com/appendix/>`__’ website) are reviewed and documented by the Audit Team using functionalities provided by their license scanning tool, and are included in each software component’s internal report generated through such tool.

As for uncommon open source licenses (and for uncommon variants of common licenses), an assessment by the Legal Team is required, which will be managed through the OSS issue tracker described in the Appendix (the “OSS Issue Tracker”); issues in this respect should be opened by any role who encounters an uncommon Identified License that appears not to have been reviewed yet in the OSS Issue Tracker or in the OSS wiki described in the Appendix (the “OSS Wiki”). The final outcome of the assessment will be included in the database of the license scanning tool by the Audit Team.

|main_project_name| projects are generally intended to be released as source only distributions. Distribution of application and/or library binaries should be generally avoided within |main_project_name| projects; however, some binary blobs could have to be distributed along with |main_project_name|, to enable compatibility with certain hardware devices and components. License obligations, restrictions and rights related to such binary blobs shall be always reviewed and assessed by the Legal Team.

Since |main_project_name| is intended to be implemented downstream by device makers, who will typically perform a binary distribution of modified parts of such software, |company_entity_shortname| commits to provide them with some basic assessment and information on license obligations, restrictions in the context of a typical binary/firmware distribution, including information about the existence and the license conditions of possible third party binary blobs.

In case of binary/firmware/proprietary software distribution, this possibility must be clearly identified in the top level documentation of |main_project_name|.

In reviewing and documenting the obligations, restrictions and rights granted by each Identified License, |company_entity_shortname| commits to make use of, and to contribute to, open source resources such as ‘`Choose a License <https://choosealicense.com/appendix/>`__’.

.. _relevant-tasks-defined-and-supported:

Relevant Tasks Defined and Supported
************************************

.. _external-open-source-inquiries:

External Open Source Inquiries
==============================

Anyone receiving an Open Source Compliance inquiry from outside the |company_entity_shortname| shall refer to the OSRB, which shall have overall responsibility for dealing with the inquiry, and – where appropriate – for assigning the handling of all or part of it to the appropriate role(s) within the company. This process will be managed through the OSS Issue Tracker.

In all public documentation concerning |main_project_name| it should be stated that all external inquires concerning open source licensing should be sent the following email address: |email_for_external_inquiries|. Email messages sent to such email address will automatically trigger the creation of an issue on the OSS Issue Tracker, which will be assigned to OSRB members who will handle it.

.. _resourcing:

Resourcing
==========

To ensure that all tasks in the Program are executed and effectively resourced, roles, responsibilities and time requirements are set forth in sec. 3.1.2.

The Compliance Officer is responsible for ensuring that adequate funding is allocated for the success and the effectiveness of the Program.

The main Open Source policy and these implementation guidelines are open to constant review and update in our git server described in the Appendix (the “Internal Git Server”). Issues about modifications and proposed enhancements should be opened in the OSS Issue Tracker.

Legal expertise from Legal Team pertaining to Open Source Compliance is accessible to any role through the OSS Issue Tracker. Before opening an issue with the Legal Team, please check in the OSS Issue Tracker and in the OSS Wiki if the issue has been already addressed before.

Any other open source compliance issue will be managed through the OSS Issue Tracker and assigned to the OSRB.

.. _open-source-content-review-and-approval:

Open Source Content Review and Approval
***************************************

.. _sec-bom:

Bill of Materials
=================

Our process for creating and managing bill of materials that includes each Open Source component (and its Identified License) distributed within the Supplied Software is based on an open source compliance software CI/CD toolchain described in the Appendix. It is intended to be a continuous process overseen by the OSRB and carried out by the Audit Team and by the Legal Team. During software development process, project bill of materials is gradually built, updated and validated, so that at release time the the toolchain should be able to automatically generate a complete BOM by reusing human validation work done during development.

.. _license-compliance:

License Compliance
==================

In the CI/CD process described at sec. 3.3.1, generally any potential legal issue should be tracked and addressed since the very beginning of software development. Software Developers, Software Team Managers, Audit Team members are required to check, whenever a new component is added to a project, or an existing third party component is modified: *(i)* what is the license attached to it, *(ii)* if there is any information concerning that component and/or its license in the OSS Wiki or in the OSS Issue Tracker; and, if they spot a potential issue or they have doubts, they shall open an issue in the OSS Issue Tracker, which will be handled by the Legal Team.

.. _compliance-artifacts-creation-and-delivery:

Compliance Artifacts Creation and Delivery
******************************************

.. _compliance-artifacts:

Compliance Artifacts
====================

The process outlined in sec. 3.3.1 allows the creation of Compliance Artifacts with automated CI/CD pipelines on our Internal Git Server – namely, a SPDX document for each software package/component and a SPDX document describing the whole project distributions, and an internal report covering internal information and legal assessment for each component.

.. _open-source-community-engagements:

Open Source Community Engagements
*********************************

.. _contributions-to-other-projects:

Contributions To Other Projects
===============================

General principles and rules covering contribution to third party open source projects are covered by our main Open Source Policy.

Contributions to third parties projects must be forked in the official account of |company_entity_shortname| and subject to the internal procedures to open repositories, forks, etc. as updated from time to time on the OSS Wiki.

Local repositories must me cloned on the internal IT services that must have been expressly cleared for this use and are approved by the Compliance Officer. Appointment to follow an external project **must not hinder compliance** with the IT security procedures in place.

.. _external-contributions-from-others-to-our-projects:

External Contributions From Others To Our Projects
==================================================

As to external contributions *to* |company_entity_shortname|’s open source project, each repository must have a ``contributing.md`` file that reflects this policy. All contributions that do not contain a sign-off statement will be rejected or will receive a request to sign-off.

This must be made an **automated** process as much as possible, integrated in the CI/CD environment of all processes hosted or initiated by |company_entity_shortname|.
