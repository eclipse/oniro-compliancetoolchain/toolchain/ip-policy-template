.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _section-1:

Appendix. 
##########

This Appendix describes the internal processes, document, resources necessary to follow the Implementation Guidelines.

**Note**: this document is a stub for users. It could contain actual content or just point to external resources.

.. _general-list-of-useful-contacts:

Appendix.1. General List of useful contacts
*******************************************

.. _training:

Appendix.2. Training
********************

.. _training-requirements-for-each-role:

Appendix.2.1. Training Requirements for Each Role
=================================================

.. _learning-platform:

Appendix.2.2. Learning Platform
===============================

Platform delivers training and keeps track of undertaken and training not undertaken by each individual.

.. _issue-tracker:

Appendix.3. Issue Tracker
*************************

.. _oss-wiki:

Appendix.4. OSS Wiki
********************

.. _description-of-integration-of-compliance-tools-in-cicd:

Appendix.5. Description of integration of compliance tools in CI/CD
*******************************************************************
