.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _definitions:

Definitions
###########

   This section is taken from Section 2 of the |openchain_specification|, Copyright 2016-2019 Linux Foundation, licensed under |openchain_specification_license|

**Compliance Artifacts**
   a collection of artifacts that represent the output of the Program for the Supplied Software. The collection may include (but is not limited to) one or more of the following: source code, attribution notices, copyright notices, copy of licenses, modification notifications, written offers, Open Source component bill of materials, and SPDX documents.
**Identified Licenses**
   a set of Open Source Software licenses identified as a result of following an appropriate method of identifying Open Source components from which the Supplied Software is comprised.
**OpenChain Conformant**
   a Program that satisfies all the Requirements of this specification.
**Open Source**
   software subject to one or more licenses that meet the Open Source Definition published by the Open Source Initiative OpenSource.org or the Free Software Definition (published by the Free Software Foundation) or similar license.
**Program**
   the set of policies, processes and personnel that manage an organization’s Open Source license compliance activities.
**Software Staff**
   any organization employee or contractor that defines, contributes to or has responsibility for preparing Supplied Software. Depending on the organization, that may include (but is not limited to) software developers, release engineers, quality engineers, product marketing and product management.
**SPDX**
   the format standard created by the Linux Foundation’s SPDX (Software Package Data Exchange) Working Group for exchanging license and copyright information for a given software package. A description of the SPDX specification can be found at www.spdx.org.
**Supplied Software**
   software that an organization distributes to third parties (e.g., other organizations or individuals).
**Verification Materials**
   materials that demonstrate that a given requirement is satisfied.
