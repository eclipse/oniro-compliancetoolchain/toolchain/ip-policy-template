<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Oniro Open Source Policy Template

Sources for OSS Policy and OSS Policy Implementation Guidelines templates that can be used and adapted for Oniro-based projects and for OSS operating system projects in general.

Document sources, in RST format, are placed within `policy/source` and `implementation_guidelines/source`.

Documents can be built with [sphinx](https://www.sphinx-doc.org)

```shell
make -C policy html
make -C implementation_guidelines html
```

Built html files are placed in `policy/build/html` and `implementation_guidelines/build`.

## License

The license of this repository is as follows:

* Documentation text is under `CC-BY-4.0` license
* Scripts, tools, and so on, are under `Apache-2.0` license

See the `LICENSES` subdirectory for full license texts.
