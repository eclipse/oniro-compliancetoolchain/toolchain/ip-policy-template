.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _introduction-why-open-source-values-are-our-values:

Introduction: Why Open Source Values are Our Values
###################################################

In a **connected world** like the one we live in, we believe that the persistence of business models based on closed source software, silo software development, lack of interoperability and vendor lock-ins is causing substantial damages to users, content providers and device manufacturers in terms of transaction costs and consequently lost opportunities.

The incredible potential offered by the digital revolution is being stifled by the incapability of smart devices from different brands to interact seamlessly and to ensure interoperability; requiring strong efforts sometimes unreasonably in making applications and contents compatible with different platforms and devices – while the same energy could be better used to improve them, and make users’ lives easier.

Today’s enhanced device and ecosystem diversity does not ensure freedom of choice: users are not free to combine different devices with each other; device manufacturers are not free to choose software components based simply on their quality and features; content creators are not free to distribute the same content or application to any device and platform.

In opposition to this model, we are building |main_project_name_bold|, a **fair and open source software ecosystem**

We truly believe that such an ambitious project can succeed only if it is true open source.

“True open source” does not mean identifying the problem, building the solution and donating that entire solution to the world. Rather, “true open source” is about **collaboration**, it is about **sharing** and discussing **ideas, plans and roadmaps** with others.

“True open source” is about **active and responsible members of a community** who share some **common fundamental values**: the freedom to use, study, share and improve software programs; the freedom of choosing technologies based only on their features and quality, and not because of vendor lock-in strategies; the value of interoperability, as a means to achieve such freedom; the values of shared learning, peer review and meritocracy, as a means to enhance developers’ skills – and get better software, too; the value of reusing others’ code while respecting their rights, in order to build a true software commons; the value of transparency, to share control on technology and protect everyone’s digital sovereignty.

**This policy is about how we want to implement these values in our organization and in the software projects we steward**.
