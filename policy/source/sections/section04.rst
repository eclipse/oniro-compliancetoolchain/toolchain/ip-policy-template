.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _sec-upstream:

Upstream First…
###############

|main_project_name| software projects typically incorporate code from other open source projects (such as software libraries, kernels, system tools, etc.) on the upstream side of the supply chain, while on the downstream side they are intended to be used as a basis to develop vertical-specific implementations (like firmware for specific IoT or smart devices).

We at |company_entity_shortname| choose to adopt an “**Upstream First**” **approach**, both for our own projects and for third party projects we incorporate in |main_project_name|.

.. _sec-upstreamfirst_own:

…for our own code/projects
**************************

For projects we directly maintain, “*Upstream First*” means that even if there are any downstream versions developed by |company_entity_shortname| (vertical-specific, device-specific, etc.), any improvements, bug/security fixes and new features will be generally made available first on the corresponding upstream projects.

Exceptions to this general principle may be allowed in case of changes required to fix embargoed and/or critical CVEs, or involving customer proprietary information or experimental features, or in case of product deadlines requiring to work on upstream and downstream at the same time.

For this reason |company_entity_shortname| developers should verify with their managers if any of the above exceptions apply before pushing any changes upstream for the first time.

.. _sec-upstreamfirst_3rdparty:

…for third party projects we incorporate in our projects
********************************************************

For third party OSS projects we incorporate or include in our projects or software distributions, the general principle is to avoid downstream forks, if at all possible: any improvements, bug/security fixes, new features should be generally offered first to the upstream project, requiring to be accepted.

Only if changes are not accepted upstream within a reasonable time, |company_entity_shortname| will take actions without delay in order to work out a viable option, taking into account long-term maintenance costs of the fork, possible loss of interoperability, and any other relevant technical reasons.

Furthermore, the same exceptions listed in sec. :ref:`4.1 <sec-upstreamfirst_own>`  above apply here, and may justify downstream forks of third party projects.

In any case, **downstream forks of third party OSS projects** included in |main_project_name| should follow common best practices, so that:

-  upstream original code and downstream modifications are clearly identifiable from each other (eg. by means of patch files and/or adequate git branching policies);
-  the authors of downstream modifications are clearly identified (this includes also original upstream authors in case of backports from upstream project’s next versions);
-  downstream version tags should be coherent with upstream version tags and should follow semantic versioning `specifications <https://semver.org>`__ and `best practices <https://github.com/semver/semver/issues/278>`__, preferably using build meta tags according to `semver spec item-10 <https://semver.org/#spec-item-10>`__

..

   For instance, if the upstream project ‘foo’ has a version tag like ‘1.0.0’, the corresponding forked version of ‘foo’ should have a tag like ‘1.0.0+|main_project_tag|.1’)
