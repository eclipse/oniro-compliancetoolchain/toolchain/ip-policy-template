.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _copyright-notice-ip-policy:

Copyright Notice. 
##################

Copyright 2021 Huawei. Licensed under `CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`__.

Authors: Carlo Piana and Alberto Pianon (`Array <https://array.eu>`__)

Reviewers: Davide Ricci and Christian Paterson (Huawei)
