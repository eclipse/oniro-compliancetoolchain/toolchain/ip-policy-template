.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _patents:

Patents
#######

.. _general:

General
*******

In several jurisdictions, software is also covered by patents. Therefore a license from the patent(s) holder is necessary for a downstream implementer.

This is in its essence contrary to the spirit open source licensing and a full release of software as open source should be made in a way that patents cannot be used to revoke the liberties that the license under which the software is released have granted.

In this section we discuss the ways patents play a role in |company_entity_shortname|’s distribution of open source software.

.. _open-invention-network:

Open Invention Network
**********************

OIN pools patents that are relevant to the specific areas covered by the Linux Definition, where the patent holders agree on a non aggression pact with all other members, creating a patent-war-free-zone limited to the scope of the `Linux Definition <https://openinventionnetwork.com/linux-system/>`__. |company_entity_shortname| endorses OIN and invites all players in the Open Source field to join the project.

.. _open-source-licenses-are-also-patent-licenses:

Open source licenses are (also) patent licenses
***********************************************

Often, open source licenses are referred to as “copyright licenses”. While extensively using copyright as a tool to grant and protect the freedoms embedded in the Open Source and Free Software Liberties, open source licenses are to be seen as conditional permissions to use the software. No matter under whatever exclusive right this permission is required, insofar as it is controlled by the licensor.

In this light, the main license chosen by |company_entity_shortname| is the |main_project_license| `license <main_project_license_url>`__. This license embeds an express patent license over the patents held by the contributor for the contributions they made.

Other licenses include some sort of patent pledge, permission, grant, license.
