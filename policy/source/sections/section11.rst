.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _trademarks:

Trademarks
##########

.. _ownership:

Ownership of signs
******************

Despite the code of |main_project_name| being Free/Libre and Open Source Software, it is important that the trademarks and the other signs be preserved in their function to identify this project. Therefore, although everyone has the right to benefit from the liberties as provided by the licenses on the code and upon compliance with their conditions, at the same time exercising these liberties does not imply nor require the use of trademarks.

Publication of the source code of |main_project_name| and of any other elements (there included images, logos in vectorial and raster format, etc.) does not imply a license to the use of trademarks and of any other identifying sign, including the specific combination of colors (“trade dress”), be they registered or unregistered signs.

The only permitted uses are those explicitly provided for and are expressly subject to the relevant trademark policy or license, if offered.

.. _permitted-uses:

Permitted uses
**************

.. _mirroring-and-forks:

Mirroring and forks
===================

A use of the distinctive signs in a reasonably updated copy of the official repository of |main_project_name| is always permitted, conditional upon:

-  a clear mention be made and an hyperlink be offered in a sufficiently prominent way to lead to the official repository, mentioning at the same time, for example in an added “mirror.txt” file, that it is a copy of the official repository, offered for convenience, or a fork.
-  if it is a fork of |main_project_name| to host future independent developments to be conferred into |main_project_name|, the ``master`` branch be at all times in alignment with the official repository and all contributions be merged to a non-owned branch only through a pull request to the official repository, and never through a local merge and push.
-  for the sake of clarity, nothing in these rules has either the scope or the effect to limit or prevent anybody from the full exploitation of the freedoms and rights conferred by the applicable license to any portion of |main_project_name|

.. _descriptive-use:

Descriptive use
===============

In general, according to the relevant law (please check) it is permitted to use signs only in a descriptive way, that is to mention |main_project_name| or certain elements thereof, under the condition that there is no arising potential confusion as to the provenance of the code, that there is no interference with the normal exploitation of the signs in their distinctive function, there is no dilution of them and – as a general rule – good faith is used. As a general rule, it is not considered in good faith to use colored logos, using the names in an identifying way without the use of clarifying elements of the descriptive use.

This section has only explanatory purposes of what the law generally considers fair use and how |company_entity_shortname| interprets it, but it bears no effect whatsoever to acknowledge any fair use beyond what the law already considers so and cannot be otherwise construed.

.. _software-heritage:

Software heritage
=================

All uses connected to preservation of software as cultural heritage, in accordance with the `Paris Call <https://en.unesco.org/foss/paris-call-software-source-code>`__ are always permitted.

.. _third-parties-tm:

Third parties’ TM
===================

This policy document has no effect over trademarks of any nature belonging to third parties.
