.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _sec-incorporating:

Incorporating Third Party Projects
##################################

When planning to add new components and functionalities, |company_entity_shortname| developers are strongly encouraged to first consider existing open source alternatives.

However, not any open source software is acceptable for inclusion in projects maintained by |company_entity_shortname|.

-  *Zero*, the component must be open source, i.e. it must come with an open source LICENSE file (see next point), and the component should not contain any known critical `CVEs <https://cve.mitre.org/>`__ (see last point).

-  *First*, the license: it should be an Open Source license, according to the definition given in :ref:`Glossary <glossary>` . Should the considered project contain components subject to different licenses, their internal coherence should be checked, to avoid inbound/outbound license incompatibilities (see :ref:`Glossary <glossary>` ).

-  *Second*, dependencies and license compatibility: if the considered component will be a dependency of, and/or will depend on other components, inbound/outbound license compatibility should be checked (see :ref:`Glossary <glossary>` ). Also other possible legal issues should be considered (for instance related to possible third party patents covering the technology).

-  *Third*, it should be checked if the project is maintained by an independent community, or by a single corporation or a foundation, weighing the pros and cons of it (eg. financial and organizational support vs. possible lack of independence from private interests that may diverge from |main_project_name| goals).

-  *Fourth*, project maturity and activity: long established projects, with frequent substantive commits, should be generally preferred over projects that are relatively new or have been inactive for a long time.

-  *Last but not least*, quality and security: projects with a relatively high number of critical open bugs, issues and CVEs should be generally avoided.
