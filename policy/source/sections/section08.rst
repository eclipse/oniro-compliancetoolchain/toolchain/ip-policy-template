.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _open-source-license-compliance:

Open Source License Compliance…
###############################

.. _sec-respect:

…is a Matter of Respect
***********************

As mentioned in the :ref:`Introduction <introduction-why-open-source-values-are-our-values>` , “True Open Source” means respecting third parties’ rights while reusing their code – which, in legal terms, means respecting open source license obligations – whilst acknowledging their efforts and respecting the ideals of the Open Source community.

As explained in sec. :ref:`5.1 <sec-OSS_is_collaboration>` , in an Open Source ecosystem **the license is the key legal interface enabling parties to give and receive software contributions**, upstream and downstream, from entities and individuals spread across different countries and organizations. **Without the license, there is no** “**True Open Source**”, both from a legal and a community perspective.

Thus, respecting the license essentially means respecting the legal terms that enable collaboration in an open source community; if one wants to engage or participate in a community, one should respect its rules, not just because they are legally binding, but to gain trust and respect, and be fully accepted into that community.

.. _oss-compliance-in-our-upstream-projects:

OSS compliance in our upstream projects…
****************************************

.. _our-own-code:

Our “Own” Code
==================

As to our own open source code included in |main_project_name| projects and software distributions (for a definition of “own” software, see the :ref:`Glossary <glossary>` , we commit to always make available the complete source code in our public repositories, following the “*Upstream First*” approach (see sec. :ref:`4 <sec-upstream>` ).

We also commit to be fully `REUSE <https://reuse.software/>`__ Compliant, in order to cut off any uncertainty about copyrights and licenses applying to our own code, therefore easing open source compliance work for |main_project_name| downstream implementers.

.. _third-party-code:

Third Party Code
================

Given the size and nature of |main_project_name| project, a number of these components typically come from third parties.

From a copyright point of view, some components may be independent works in themselves – or, in the GPL parlance, “mere-aggregation” artifacts – while some others may form a derivative work, depending on the technical aspects of their dependency relationship (see :ref:`Glossary <glossary>` ). In the case of derivative works, not all combinations of components are allowed: inbound component licenses must be compatible with each other *and* with the outbound license of the whole derivative work (see :ref:`Glossary <glossary>` ), and, in case of license incompatibilities, the whole derivative work cannot be distributed (because it would violate the license terms of one or more components).

Therefore, with regards third party open source components included in |main_project_name| projects, we commit to **perform a dependency mapping and a legal analysis on third party code on a regular basis via CI/CD**, in order to **avoid license incompatibility problems from the earliest possible moment during project development** and resolve eventual incompatibilities if they are seen to arise. Such legal analysis will also enable us to **identify and meet** (and to help downstream implementers to meet) all **license obligations related to third party components**.

.. _accepted-licenses:

Accepted Licenses
=================

As a general rule, while we give preference to |main_project_license|, we will accept any licenses for third party components to be included in |main_project_name| projects and software distributions, as long as they are Open Source Licenses, according to the definition given in :ref:`Glossary <glossary>` .

Instead of prohibiting particular licenses to be included in |main_project_name|, we commit to provide our developers with guidelines and procedures to avoid prohibited *combinations* of software components due to license incompatibilities, as described above.

.. _oss-compliance-for-downstream-implementers:

OSS compliance for Downstream Implementers
******************************************

We commit to provide device manufacturers and other |main_project_name| downstream implementers with metadata and open source tools to ease their compliance work and to help them generate BOMs and compliance artifacts (see :ref:`Glossary <glossary>` ) for their software and firmware distributions.

We are aware that some downstream implementers may need, for legal or other internal reasons, to rule out components subject to certain open source licenses from their software/firmware distributions. Thus we commit to include in |main_project_name| appropriate tools and configuration options to that purpose, and to make reasonable efforts to provide if possible a choice of alternative components subject to different licenses, whenever it is reasonable and technically feasible to do so.

.. _reproducible-builds:

Reproducible Builds
*******************

|company_entity_shortname| commits – whenever it is reasonable to do so – to follow the best practices provided by the `Reproducible Builds <https://reproducible-builds.org/>`__ project.
