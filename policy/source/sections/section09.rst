.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _proprietary-drivers-and-object-code:

Proprietary Drivers and object code
###################################

.. _the-rule:

The rule
********

|main_project_name| strives to be 100% Free and Open Source Software.

However, in certain cases, there is no viable alternative than accepting a **proprietary, binary-only** contribution – at least as a temporary solution, such as proprietary firmware. This should be an exception and it needs to be deeply reviewed by the Open Source Executive Committee.

Generally, no proprietary software may be included in |main_project_name|, and in any case no proprietary software that may lead to license incompatibilities (eg. kernel modules).

When *both* proprietary and FLOSS drivers are available for a specific device, upstream |main_project_name| distribution will include *only* FLOSS drivers (even if they are less performant or have less functionalities); instructions to download proprietary drivers downstream may be provided to device makers, but

-  they must be downloaded separately by the downstream implementer
-  the proprietary driver must be known not to raise compliance issues for those downloading it: it shall however be the downstream implementer’s duty to check thoroughly and a full disclaimer must be included that **due diligence** is upon them
-  |main_project_name| never knowingly advises to use malicious software or software containing know major vulnerabilities
-  the rest of the policies with regard to third parties software must be complied to the maximum extent
-  the downloadable software must not be a dependency of |main_project_name| under no conditions: if this is the case, sec. :ref:`9.2 <sec-sec_exception>`  should be followed.

.. _sec-sec_exception:

The exception
*************

Exceptionally, proprietary firmware/bootloaders may be included in |main_project_name| for hardware compatibility reasons only (i.e. when there is no other viable option to get a build that manages to boot on a specific device), in one of the following ways, in this order of preference:

a) Convince the IP vendor to upstream the firmware to some package such as ``linux-firmware`` that allows for redistribution;

b) Enter into licensing agreement with the IP owner in order to acquire sufficient redistribution rights and the right to pass them through; this must be sufficient so that |main_project_name| images can incorporate the proprietary firmware and boot out of the box.

c) **If nothing else is workable**, a scripted workflow that allows a user to download |main_project_tag| in a temporary version, which may not even boot on a device, download the proprietary firmware on the user’s machine, prompt the user for an EULA, and then incorporate the firmware into the image. The last part is usually achieved by loop mounting the image locally and adding the necessary files. The presence of this download mechanism and the requirement to sign an EULA **must be clearly advertised at download time**, at latest.

.. _notices:

Notices
*******

The presence of proprietary files must be sufficiently advertised in the repository or in the repository’s directory where firmware is placed.
