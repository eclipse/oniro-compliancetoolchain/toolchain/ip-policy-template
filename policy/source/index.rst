.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. Huawei Open Source Policy documentation master file, created by
   sphinx-quickstart on Fri Nov 20 12:21:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: definitions.rst

Open Source Policy
==================

.. toctree::
   :glob:

   sections/*
