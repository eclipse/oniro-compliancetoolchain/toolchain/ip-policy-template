.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. |policy_publication_date| replace:: January 15th 2021
.. |policy_entry_into_force_date| replace:: March 22nd 2021
.. |main_project_name| replace:: Oniro-based Project
.. |main_project_name_bold| replace:: **Oniro-based Project**
.. |main_project_tag| replace:: Oniro Based Project
.. |main_project_license| replace:: "Apache Public License v. 2.0"
.. |company_entity| replace:: ACME Inc.
.. |company_entity_shortname| replace:: ACME
.. |company_entity_shortname_bold| replace:: **ACME**
.. |openchain_conformance_timing| replace:: ACME Inc. has undertaken a path to **conform to OpenChain 2.0 specifications (ISO/IEC 5230)**, which is planned to be completed by the end of 2021.
.. |legal| replace:: the internal Legal Team
.. |ip_expert| replace:: the internal IP experts
